/*
 * Copyright 2014 FXPlayground.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fxplayground.services;

import com.sun.javafx.css.StyleManager;
import javafx.animation.TranslateTransition;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SubScene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import javafx.util.Duration;
import javafx.util.StringConverter;
import netscape.javascript.JSObject;
import org.fxplayground.beans.CompilerException;
import org.fxplayground.beans.FXPGResource;
import org.fxplayground.beans.Language;
import org.fxplayground.beans.Playground;
import org.fxplayground.utils.InMemURLStreamHandler;
import org.fxplayground.views.SplitViews;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <pre>
 * +---+------+--------+
 * |   |      |        |
 * | A |  B   |   C    |
 * |   |      +--------+
 * |   +------|        |
 * |   |      |        |
 * |   |  D   |   E    |
 * |   |      |        |
 * +---+------+--------+
 *
 * A - vertical left is the project options. (projectSettingsView)
 * B - fxml/html window           splitViews
 * C - CSS window                 splitViews
 * D - language window            splitViews
 * E - output window              splitViews
 * </pre>
 * User: cdea
 * Date: 7/1/2014
 */
public class DefaultFXPGViewManagerImpl implements FXPGViewManager {
    private static Logger logger = Logger.getLogger(DefaultFXPGViewManagerImpl.class.getName());
    private static Logger scriptLogger = Logger.getLogger("org.fxplayground.user.code");
    private SplitViews splitViews;
    private Pane projectSettingsView;
    private static final String PROJECT_NAME_FIELD = "project-name";
    private static final String PROJECT_DESC_FIELD = "project-description";
    private static final String PROJECT_USE_FXML_CHBOX = "project-use-fxml-checkbox";
    private static final String PROJECT_DETACH_CHBOX = "project-detach-checkbox";
    private static final String FXML_PROJECT_SETTINGS_FILE = "/fxplayground_options.fxml";
    private static final String CLOSE_SETTINGS_BUTTON = "close-settings-button";
    private static final String JAR_LIBRARY_FIELD = "jar-library-textfield";
    private static final String JAR_LIBRARY_LISTVIEW = "jar-library-listview";
    private static final String JAR_LIBRARY_ADD_BUTTON = "jar-library-add-button";
    private static final String JAR_LIBRARY_REMOVE_BUTTON = "jar-library-remove-button";
    private static final String JAR_LIBRARY_MOVE_UP_BUTTON = "jar-library-up-button";
    private static final String JAR_LIBRARY_MOVE_DOWN_BUTTON = "jar-library-down-button";
    private static final String MARKUP_LANGUAGE_COMBOBOX = "markup-language-combobox";
    private static final String CODE_LANGUAGE_COMBOBOX = "code-language-combobox";

    private static final String SETTINGS_ACCORDION = "settings-accordion";

    /* All child nodes to be referenced quickly. */
    private Map<String, Node> projectSettingChildNodeMap = new HashMap<String, Node>();
    /* When the project settings view is open or closed */
    private BooleanProperty projectSettingsViewToggleOpen = new SimpleBooleanProperty(false);

    /* compile error line */
    private int lineNumber = -1;
    /* last parent directory a jar was selected */
    private File previousJarDirectory;
    static String urlString = null; // where fxplayground (and java) was launched. Working directory.
    static {
            URL url = null;

            try {
                url = new File(System.getProperty("user.dir")).toURI().toURL();
                urlString = url.toURI().toURL().toString();
            } catch (MalformedURLException e) {
                logger.log(Level.SEVERE, "Unable to obtain user's home directory.", e);
            } catch (URISyntaxException e) {
                logger.log(Level.SEVERE, "Unable to obtain user's home directory.", e);
            }
    }
    public String getDomEditorText() {
        Object contents = getSplitViews().getDomEditor().getEngine().executeScript("editor.getValue()");
        return String.valueOf(contents);
    }
    public String getLanguageEditorText() {
        Object contents = getSplitViews().getLanguageEditor().getEngine().executeScript("editor.getValue()");
        return String.valueOf(contents);
    }

    public String getCssEditorText() {
        Object contents = getSplitViews().getCssEditor().getEngine().executeScript("editor.getValue()");
        return String.valueOf(contents);
    }

    @Override
    public void setDomEditorText(String text, String markupLanguage) {
        JSObject jsobj = (JSObject) getSplitViews().getDomEditor().getEngine().executeScript("window");
        jsobj.setMember("domText", text);

        String langMode = "xml";
        if ("HTML".equalsIgnoreCase(markupLanguage)) {
            langMode = "html";
        }
        getSplitViews().getDomEditor().getEngine().executeScript("editor.getSession().setMode(\"ace/mode/" + langMode + "\");");
        getSplitViews().getDomEditor().getEngine().executeScript("editor.setValue(domText)");
        getSplitViews().getDomEditor().getEngine().executeScript("editor.navigateFileStart()");
    }
    private String getWorkingDirectoryUrl() {
        URL url = null;
        String urlString = null;
        try {
            url = new File(System.getProperty("user.dir")).toURI().toURL();
            urlString = url.toURI().toURL().toString();
        } catch (MalformedURLException e) {
            logger.log(Level.SEVERE, "Unable to obtain user's working directory.", e);
        } catch (URISyntaxException e) {
            logger.log(Level.SEVERE, "Unable to obtain user's working directory.", e);
        }
        return urlString;
    }

    @Override
    public void setLanguageEditorText(String text, Language language) {
        JSObject jsobj = (JSObject) getSplitViews().getLanguageEditor().getEngine().executeScript("window");
        jsobj.setMember("codeText", text);
        getSplitViews().getLanguageEditor().getEngine().executeScript("editor.getSession().setMode(\"ace/mode/" + language.getEditorInfo() + "\");");
        getSplitViews().getLanguageEditor().getEngine().executeScript("editor.setValue(codeText)");
        getSplitViews().getLanguageEditor().getEngine().executeScript("editor.navigateFileStart()");
    }

    @Override
    public void setCssEditorText(String text) {
        JSObject jsobj = (JSObject) getSplitViews().getCssEditor().getEngine().executeScript("window");
        jsobj.setMember("cssText", text);
        getSplitViews().getCssEditor().getEngine().executeScript("editor.setValue(cssText)");
        getSplitViews().getCssEditor().getEngine().executeScript("editor.navigateFileStart()");
    }

    public SplitViews getSplitViews() {
        if (splitViews == null) {
            splitViews = new SplitViews();
        }
        return splitViews;
    }

    public Pane getProjectSettingsView() {
        if (projectSettingsView == null) {
            projectSettingsView = createProjectSettingsView();
        }
        return projectSettingsView;
    }
    private Pane createProjectSettingsView() {
        try {
            AnchorPane projectSettingsView = FXMLLoader.load(getClass().getResource(FXML_PROJECT_SETTINGS_FILE));

            // TODO: Decide to allow the user to change these positional values?
            StackPane.setAlignment(projectSettingsView, Pos.TOP_LEFT);
            projectSettingsView.setMaxSize(AnchorPane.USE_PREF_SIZE, AnchorPane.USE_PREF_SIZE);
            populateProjectSettingsNodeMap(projectSettingsView);
            projectSettingsView.setUserData(projectSettingsViewToggleOpen);
//             https://javafx-jira.kenai.com/browse/RT-24367
//            ListView<FXPGResource> jarLibsListView = lookup(projectSettingChildNodes, JAR_LIBRARY_LISTVIEW, ListView.class);
//            projectSettingChildNodeMap.put(JAR_LIBRARY_LISTVIEW, jarLibsListView);
////            jarLibsListView.setCellFactory(new Callback<ListView<FXPGResource>, ListCell<FXPGResource>>() {
////                @Override
////                public ListCell<FXPGResource> call(ListView<FXPGResource> param) {
////                    Label jarLbl = new Label();
////                    Tooltip tooltip = new Tooltip();
////                    ListCell<FXPGResource> cell = new ListCell<FXPGResource>(){
////                        public void updateItem(FXPGResource item, boolean empty){
////                            super.updateItem(item, empty);
////                            if (item != null) {
////                                jarLbl.setText(item.getUrl());
////                                setText(item.getName());
////                                tooltip.setText(item.getUrl());
////                                setTooltip(tooltip);
////                            }
////                        }
////                    };
////                    return cell;
////                }
////            });
            return projectSettingsView;
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Unable to create the Project Settings View.", e);
        }
        return null;
    }

    private boolean updateLanguageEditor = false;
    @Override
    public void wireupPlaygroundOptionsUI(Playground playground, List<Language> languages) {
        TextField projNameField = findNodeById(PROJECT_NAME_FIELD);
        playground.nameProperty().bindBidirectional(projNameField.textProperty());

        TextArea projDescField = findNodeById(PROJECT_DESC_FIELD);
        playground.descriptionProperty().bindBidirectional(projDescField.textProperty());

        CheckBox useFXMLChBox = findNodeById(PROJECT_USE_FXML_CHBOX);
        playground.useFXMLProperty().bindBidirectional(useFXMLChBox.selectedProperty());

        CheckBox detachChBox = findNodeById(PROJECT_DETACH_CHBOX);
        playground.detachOutputProperty().bindBidirectional(detachChBox.selectedProperty());

        // slide setting panel to close
        Button closeButton = findNodeById(CLOSE_SETTINGS_BUTTON);
        closeButton.setOnAction(actionEvent -> {

            TranslateTransition translateTransition = new TranslateTransition(Duration.millis(300), projectSettingsView);
            translateTransition.setFromX(0);
            translateTransition.setToX(-projectSettingsView.getWidth());
            translateTransition.setOnFinished(t -> {
                projectSettingsView.setVisible(false);
                // toggle project settings window to closed 'false'
                ((BooleanProperty) projectSettingsView.getUserData()).set(false);
            });
            translateTransition.play();

        });

        // Allow user to enter a url such as an http address
        TextField fileChosenTxtFld = findNodeById(JAR_LIBRARY_FIELD);
        ListView<FXPGResource> jarLibsListView = findNodeById(JAR_LIBRARY_LISTVIEW);
        jarLibsListView.setItems(playground.getResources());
        fileChosenTxtFld.setOnAction(ae -> {
            String name = fileChosenTxtFld.getText()
                                          .substring(fileChosenTxtFld.getText()
                                                  .lastIndexOf("/"));
            jarLibsListView.getItems().add(new FXPGResource(name.substring(1), "", fileChosenTxtFld.getText()));
            fileChosenTxtFld.setText("");
        });

        // popup filechooser
        Button fileChooserJarLibBtn = findNodeById(JAR_LIBRARY_ADD_BUTTON);
        fileChooserJarLibBtn.setOnAction(ae -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Resource File");
            fileChooser.getExtensionFilters()
                    .add(new FileChooser.ExtensionFilter("JAR Libraries", "*.jar"));
            if (previousJarDirectory != null) {
                fileChooser.setInitialDirectory(previousJarDirectory);
            }
            Window stageWindow = fileChooserJarLibBtn.getScene().getWindow();
            List<File> selectedFiles = fileChooser.showOpenMultipleDialog(stageWindow);
            if (selectedFiles != null) {
                selectedFiles.forEach(selectedFile -> {
                    String name = null;
                    try {
                        String fullUrlPathName = selectedFile.toURI()
                                .toURL()
                                .toString();
                        name = fullUrlPathName.substring(fullUrlPathName.lastIndexOf("/"));
                        jarLibsListView.getItems().add(new FXPGResource(name.substring(1), "", fullUrlPathName));

                    } catch (MalformedURLException e) {
                        logger.log(Level.SEVERE, "Unable to load jar library resource.", e);
                    }
                });
                if (selectedFiles.size() > 0) {
                    previousJarDirectory = selectedFiles.get(0).getParentFile();
                }
            }
        });

        // remove the selected jar file
        Button removeJarLibBtn = findNodeById(JAR_LIBRARY_REMOVE_BUTTON);
        removeJarLibBtn.setOnAction( ae -> {
            FXPGResource resource = jarLibsListView.getSelectionModel().getSelectedItem();
            playground.getResources().remove(resource);
            jarLibsListView.getItems().remove(resource);
            jarLibsListView.getSelectionModel().clearSelection();
        });

        // moves selected jar file up the list
        Button upJarLibBtn = findNodeById(JAR_LIBRARY_MOVE_UP_BUTTON);
        upJarLibBtn.setOnAction( ae -> {
            FXPGResource resource = jarLibsListView.getSelectionModel().getSelectedItem();
            int index = playground.getResources().indexOf(resource);
            if (index > 0) {
                playground.getResources().remove(resource);
                playground.getResources().add(index -1, resource);
                jarLibsListView.getSelectionModel().clearSelection();
                jarLibsListView.getSelectionModel().select(resource);
            }
        });

        // moves selected jar file down the list
        Button downJarLibBtn = findNodeById(JAR_LIBRARY_MOVE_DOWN_BUTTON);
        downJarLibBtn.setOnAction( ae -> {
            FXPGResource resource = jarLibsListView.getSelectionModel().getSelectedItem();
            int index = playground.getResources().indexOf(resource);
            if (index < (playground.getResources().size() -1)) {
                playground.getResources().remove(resource);
                playground.getResources().add(index +1, resource);
                jarLibsListView.getSelectionModel().clearSelection();
                jarLibsListView.getSelectionModel().select(resource);
            }
        });

        // Obtain the markup language combo box.
        ComboBox<String> markupLanguageComboBox = findNodeById(MARKUP_LANGUAGE_COMBOBOX);
        ObservableList<String> observableMarkupLangList = FXCollections.observableArrayList("FXML","HTML");
        markupLanguageComboBox.itemsProperty().get().clear();
        markupLanguageComboBox.itemsProperty().set(observableMarkupLangList);
        playground.markupLanguageProperty().bindBidirectional(markupLanguageComboBox.valueProperty());
        // default markup language
        playground.markupLanguageProperty().set("FXML");

        markupLanguageComboBox.valueProperty().addListener((obs, oldValue, newValue) -> {
            //Playground pl = FXPGFactory.currentPlayground();
            String editorText  = getDomEditorText();
            String tmpOld = null;
            if (oldValue != null) {
                tmpOld = oldValue;
            }
            String tmpNew = null;
            if (newValue != null) {
                tmpNew = newValue;
            }
            logger.log(Level.FINE, "----------------->" + tmpOld + "  newValue: " + tmpNew);
            if (newValue != null) {
                setDomEditorText(editorText, newValue);
            }
        });


        // set the code languages in the combo box.
        ComboBox<Language> languageComboBox = findNodeById(CODE_LANGUAGE_COMBOBOX);
        languageComboBox.converterProperty().setValue(new StringConverter<Language>() {
            @Override
            public String toString(Language lang) {
                if (lang == null) return null;
                return lang.getName();
            }

            @Override
            public Language fromString(final String langStr) {
                Optional<Language> optional = languageComboBox.getItems()
                                .stream()
                                .filter( p -> p.getName().equals(langStr))
                                .findFirst();
                return optional.get();
            }
        });
        ObservableList<Language> observableLangList = FXCollections.observableArrayList(languages);
        languageComboBox.itemsProperty().get().clear();
        languageComboBox.itemsProperty().set(observableLangList);
        playground.codeLanguageProperty().bindBidirectional(languageComboBox.valueProperty());

        if (!updateLanguageEditor) {
            updateLanguageEditor = true;
            languageComboBox.valueProperty().addListener((obs, oldValue, newValue) -> {
//                Playground pl = FXPGFactory.currentPlayground();
//            if (newValue == null) {
//                setLanguageEditorText(pl.getCodeText(), oldValue);
//            } else {
//                setLanguageEditorText(pl.getCodeText(), newValue);
//            }
                String editorText = getLanguageEditorText();
                String tmpOld = null;
                if (oldValue != null) {
                    tmpOld = oldValue.getEditorInfo();
                }
                String tmpNew = null;
                if (newValue != null) {
                    tmpNew = newValue.getEditorInfo();
                }
                logger.log(Level.FINE, "----------------->" + tmpOld + "  newValue: " + tmpNew);
                if (newValue != null) {
                    setLanguageEditorText(editorText, newValue);
                }
            });
        }
        // open first titled pane
        Accordion accordion = findNodeById(SETTINGS_ACCORDION);
        accordion.setExpandedPane(accordion.getPanes().get(0));
    }

    @Override
    public void unWireupPlaygroundOptionsUI(Playground playground) {
        TextField projNameField = findNodeById(PROJECT_NAME_FIELD);
        playground.nameProperty().unbindBidirectional(projNameField.textProperty());

        // slide setting panel to close
        Button closeButton = findNodeById(CLOSE_SETTINGS_BUTTON);
        closeButton.setOnAction(null);

        // Allow user to enter a url such as an http address
        TextField fileChosenTxtFld = findNodeById(JAR_LIBRARY_FIELD);
        ListView<FXPGResource> jarLibsListView = findNodeById(JAR_LIBRARY_LISTVIEW);
        jarLibsListView.setItems(null);
        //jarLibsListView.getItems().clear();
        fileChosenTxtFld.setOnAction(null);

        // popup filechooser
        Button fileChooserJarLibBtn = findNodeById(JAR_LIBRARY_ADD_BUTTON);
        fileChooserJarLibBtn.setOnAction(null);

        // remove the selected jar file
        Button removeJarLibBtn = findNodeById(JAR_LIBRARY_REMOVE_BUTTON);
        removeJarLibBtn.setOnAction(null);

        // moves selected jar file up the list
        Button upJarLibBtn = findNodeById(JAR_LIBRARY_MOVE_UP_BUTTON);
        upJarLibBtn.setOnAction(null);

        // moves selected jar file down the list
        Button downJarLibBtn = findNodeById(JAR_LIBRARY_MOVE_DOWN_BUTTON);
        downJarLibBtn.setOnAction(null);

        // open first titled pane
        Accordion accordion = findNodeById(SETTINGS_ACCORDION);
        accordion.setExpandedPane(accordion.getPanes().get(0));

        ComboBox<Language> languageComboBox = findNodeById(CODE_LANGUAGE_COMBOBOX);
        languageComboBox.itemsProperty().get().clear();
        logger.log(Level.FINE, "clear languageComboBox====> " + languageComboBox.itemsProperty().get());
        playground.codeLanguageProperty().unbindBidirectional(languageComboBox.valueProperty());
    }


    public <T> T findNodeById(String id) {
        return (T) projectSettingChildNodeMap.get(id);
    }

    /**
     * Initialize the map of controls. This is a work around for Node#lookup() bugs.
     * @param parent
     */
    private void populateProjectSettingsNodeMap(Node parent) {
        List<Node> nodes = getAllNodesWithIds(parent);
        nodes.forEach(node -> {
            if (node.getId() != null) {
                projectSettingChildNodeMap.put(node.getId(), node);
            }
        });
    }
    /**
     * One of the worst hacks i've done... Node#lookup() just couldn't search nested things.
     * @param parent
     * @return
     */
    private List<Node> getAllNodesWithIds(Node parent) {
        List<Node> nodes = new ArrayList<>();
        AnchorPane a = (AnchorPane) parent;
        nodes.add(a);
        BorderPane b= (BorderPane)a.getChildren().get(0);
        nodes.add(b);
        VBox c = (VBox) b.getCenter();
        nodes.add(c);
        nodes.addAll(c.getChildren());

        Accordion d = (Accordion) c.getChildren().get(1);
        nodes.addAll(d.getPanes());

        AnchorPane content1 = (AnchorPane) d.getPanes().get(0).getContent();
        VBox v = (VBox) content1.getChildren().get(0);
        nodes.addAll(v.getChildren());

        AnchorPane content2 = (AnchorPane) d.getPanes().get(1).getContent();
        GridPane gr = (GridPane) content2.getChildren().get(0);
        nodes.addAll(gr.getChildren());
        VBox v2 = (VBox) gr.getChildren().get(3);
        nodes.add(v2);
        nodes.addAll(v2.getChildren());

        AnchorPane content3 = (AnchorPane) d.getPanes().get(2).getContent();
        GridPane gr2 = (GridPane) content3.getChildren().get(0);
        nodes.addAll(gr2.getChildren());

        Button closeButton = (Button) b.getRight();
        nodes.add(closeButton);

        return nodes;
    }
    public void applyStylesheet(Scene scene, String url){
        scene.getStylesheets().remove(url);
        scene.getStylesheets().add(url);
    }

    public void applyStylesheet(Scene scene, String url, String contents){
        InMemURLStreamHandler.registerURL(url, String.valueOf(contents));
        scene.setUserAgentStylesheet(url);
    }
    public void applyStylesheet(SubScene subScene, String url){
        subScene.setUserAgentStylesheet(url);
    }
    public void applyStylesheet(SubScene subScene, String url, String contents){
        StyleManager styleManager = StyleManager.getInstance();
        styleManager.forget(subScene);

        subScene.getRoot().getStylesheets().remove(url);
        InMemURLStreamHandler.registerURL(url, String.valueOf(contents));
        subScene.getRoot().getStylesheets().add(url);
    }
    public SubScene getOutputView(){
           return getSplitViews().getOutputSubScene();
    }


    @Override
    public void addErrorIndicator(String editorName, int col, int line, String errMsg) {
        String code = "editor.navigateTo("+ (line - 1) + "," + col + ");\n" +
                "editor.scrollToRow(" + (line -1) + ");\n" +
                "editor.getSession().addGutterDecoration(" +(line - 1) + ", 'ace_error');\n" ;
        lineNumber = line -1;
        getSplitViews().getLanguageEditor().getEngine().executeScript(code);
    }

    @Override
    public void clearErrorIndicator(String editorName) {
        String code = "editor.getSession().removeGutterDecoration(" + lineNumber +", 'ace_error');\n" ;
        getSplitViews().getLanguageEditor().getEngine().executeScript(code);
    }

    @Override
    public void addErrorIndicator(CompilerException compilerException) {
        // add an error icon beside compile or exception line in code.
        int col = compilerException.getColumnNumber();
        int line = compilerException.getLineNumber();
        String errMsg = compilerException.getLocalizedMessage();
        addErrorIndicator(LANGUAGE_EDITOR, col, line, errMsg);
        logger.log(Level.FINE, "Script Error at line# "+ line + "col#" + col, compilerException);
        scriptLogger.log(Level.SEVERE, "Script Error at line# " + line + " col# " + col, compilerException);
    }
}
