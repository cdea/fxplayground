/*
 * Copyright 2014 FXPlayground.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fxplayground.services;

import javafx.scene.Scene;
import javafx.scene.SubScene;
import javafx.scene.layout.Pane;
import org.fxplayground.beans.CompilerException;
import org.fxplayground.beans.Language;
import org.fxplayground.beans.Playground;
import org.fxplayground.views.SplitViews;

import java.util.List;

/**
 * User: cdea
 * Date: 7/1/2014
 */
public interface FXPGViewManager {
    public static String DOM_EDITOR = "DOM_EDITOR";
    public static String LANGUAGE_EDITOR = "LANGUAGE_EDITOR";
    public static String CSS_EDITOR = "CSS_EDITOR";

    String getDomEditorText();

    String getCssEditorText();

    String getLanguageEditorText();

    void setDomEditorText(String text, String markupLanguage);

    void setCssEditorText(String text);

    void setLanguageEditorText(String text, Language language);

    SplitViews getSplitViews();

    Pane getProjectSettingsView();

    SubScene getOutputView();

    void applyStylesheet(Scene scene, String url);

    void applyStylesheet(Scene scene, String url, String contents);

    void applyStylesheet(SubScene subScene, String url);

    void applyStylesheet(SubScene subScene, String url, String contents);

    void wireupPlaygroundOptionsUI(Playground playground, List<Language> codeLanguages);
    void unWireupPlaygroundOptionsUI(Playground playground);

    public <T> T findNodeById(String id);
    void addErrorIndicator(String editorName, int col, int line, String errMsg);
    void addErrorIndicator(CompilerException compilerException);
    void clearErrorIndicator(String editorName);
}
