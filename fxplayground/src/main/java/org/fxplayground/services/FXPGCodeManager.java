/*
 * Copyright 2014 FXPlayground.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fxplayground.services;

import org.fxplayground.beans.CompiledResult;
import org.fxplayground.beans.Language;

import java.util.List;
import java.util.Map;

/**
 * User: cdea
 * Date: 6/30/2014
 */
public interface FXPGCodeManager {
    List<Language> getSupportedLanguages();
    Language findByName(String engineName);
    CompiledResult compile(Language language, String sourceCode, Map<String, Object> bindings, String displaySurfaceVariableName, String displaySurfaceNodeId, ClassLoader classLoader);
}
