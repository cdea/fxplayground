/*
 * Copyright 2014 FXPlayground.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fxplayground;

import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.ListChangeListener;
import javafx.concurrent.Worker;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebView;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.fxplayground.beans.CompiledResult;
import org.fxplayground.beans.FXPGResource;
import org.fxplayground.beans.Language;
import org.fxplayground.beans.Playground;
import org.fxplayground.services.FXPGCodeManager;
import org.fxplayground.services.FXPGFactory;
import org.fxplayground.services.FXPGViewManager;
import org.fxplayground.utils.InMemURLStreamHandler;
import org.fxplayground.utils.StreamUtil;
import org.fxplayground.utils.TemplateUtil;
import org.fxplayground.views.SplitViews;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.*;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.fxplayground.services.FXPGViewManager.LANGUAGE_EDITOR;

/**
 * User: cdea
 * Date: 6/15/2014
 */
public class Main extends Application {
    private static Logger logger = Logger.getLogger(Main.class.getName());
    private static Logger markupLogger = Logger.getLogger("org.fxplayground.user.markup");
    //private static Logger cssLogger = Logger.getLogger("org.fxplayground.user.css");

    // The application's stage
    Stage primaryStage;

    /* The current in-memory protocol and file name. */
    final static String FX_PLAYGROUND_CSS = "inmemory:fxp.css";

    /* SubScene node used to display the output */
    final static String DISPLAY_SCENE = "DISPLAY_SCENE";
    /* SubScene node's id */
    final static String DISPLAY_SCENE_NODE_ID = "display-scene";

    /* A user defined pane or group. The SubScene's root node used to display output. */
    final static String DISPLAY_SURFACE = "DISPLAY_SURFACE";
    /* A user defined pane or group using a predefined id */
    final static String DISPLAY_SURFACE_NODE_ID = "display-surface";

    /* The user defined variable name used to be used. ie DISPLAY_SURFACE.getChildren().add(FXML_NODE) */
    final static String FXML_NODE = "FXML_NODE";

    /* The current in-memory protocol and file name for fxml. */
    final static String FX_PLAYGROUND_FXML = "inmemory:markup.fxml";

    /* FXPlaygrounds home directory */
    final static String FX_PLAYGROUND_DIR = "FXPlaygrounds";
    int cnt = -1;
    private static ClassLoader classLoaderCached;

    private static Language DEFAULT_LANGUAGE;

    public static void main(String[] args) throws Exception {

        launch(args);
        logger.setLevel(Level.FINE);
        logger.addHandler(new ConsoleHandler());
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        primaryStage.setTitle("FX playground");
        FXPGViewManager viewManager = FXPGFactory.viewManager();
        FXPGCodeManager codeManager = FXPGFactory.codeManager();
        Playground playground = FXPGFactory.currentPlayground();
        DEFAULT_LANGUAGE = codeManager.findByName("nashorn");
        playground.setCodeLanguage(DEFAULT_LANGUAGE);
        SplitViews splitViews = viewManager.getSplitViews();
        MenuBar menuBar = buildMenu();
        splitViews.setTop(menuBar);

        StackPane stackPane = new StackPane(splitViews);
        Scene scene = new Scene(stackPane, 1024, 800);
        splitViews.currentEditorProperty().addListener( (o, ov, nv) -> {
            logger.log(Level.FINE, "Java side detect focus window: " + splitViews.getCurrentEditorProperty());
        });

        primaryStage.setScene(scene);
        primaryStage.show();

        // TODO: make this a binding on the form to allow user to decide language

        Pane projSettingsView = viewManager.getProjectSettingsView();
        wireupUI();
        projSettingsView.translateYProperty().bind(menuBar.heightProperty());
        projSettingsView.setVisible(false);
        stackPane.getChildren().add(projSettingsView);
        projSettingsView.setTranslateX(projSettingsView.getWidth());
    }

    private ListChangeListener clearClassloaderCache = new ListChangeListener<FXPGResource>() {
        @Override
        public void onChanged(Change<? extends FXPGResource> c) {
            classLoaderCached = null;
        }
    };

    private void wireupUI() {
        FXPGViewManager viewManager = FXPGFactory.viewManager();
        Playground playground = FXPGFactory.currentPlayground();

        viewManager.wireupPlaygroundOptionsUI(playground, FXPGFactory.codeManager().getSupportedLanguages());
        playground.getResources().addListener(clearClassloaderCache);
    }
    private void unWireupUI() {
        FXPGViewManager viewManager = FXPGFactory.viewManager();
        Playground playground = FXPGFactory.currentPlayground();
        viewManager.unWireupPlaygroundOptionsUI(playground);
        playground.getResources().removeListener(clearClassloaderCache);
        // remove all listeners.
    }

    private MenuBar buildMenu() {
        Playground playground = FXPGFactory.currentPlayground();
        MenuBar menuBar = new MenuBar();
        Menu fileMenu = new Menu("File");
        MenuItem newMenuItem = new MenuItem("_New");
        newMenuItem.setMnemonicParsing(true);
        newMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.SHORTCUT_DOWN));
        newMenuItem.setOnAction(actionEvent -> newPlayground(new Playground()));

        MenuItem openMenuItem = new MenuItem("_Open");
        openMenuItem.setMnemonicParsing(true);
        openMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.O, KeyCombination.SHORTCUT_DOWN));
        openMenuItem.setOnAction(actionEvent -> openPlayground());

        MenuItem saveMenuItem = new MenuItem("_Save");
        saveMenuItem.setMnemonicParsing(true);
        saveMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN));
        saveMenuItem.setOnAction(actionEvent -> savePlayground());

        MenuItem saveAsMenuItem = new MenuItem("Save _As");
        saveAsMenuItem.setDisable(true);
        saveAsMenuItem.setMnemonicParsing(true);
        saveAsMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN));

        MenuItem settingsMenuItem = new MenuItem("Set_tings");
        settingsMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.T, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN ));
        settingsMenuItem.setMnemonicParsing(true);
        settingsMenuItem.setOnAction(actionEvent -> toggleProjectSettingsSlideOut());

        fileMenu.getItems().addAll(newMenuItem, openMenuItem, saveMenuItem, saveAsMenuItem);
        fileMenu.getItems().addAll(new SeparatorMenuItem(), settingsMenuItem);

        Menu runMenu = new Menu("Run");
        MenuItem runMenuItem = new MenuItem("_Run");
        runMenuItem.setMnemonicParsing(true);
        runMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.R, KeyCombination.SHORTCUT_DOWN));
        runMenuItem.setOnAction(actionEvent -> dispatchRunCode());

        MenuItem debugMenuItem = new MenuItem("_Debug");
        debugMenuItem.setDisable(true);
        debugMenuItem.setMnemonicParsing(true);
        debugMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.D, KeyCombination.SHORTCUT_DOWN));
        debugMenuItem.setOnAction(actionEvent -> dispatchRunCode());
        runMenu.getItems().addAll(runMenuItem, debugMenuItem);

        menuBar.getMenus().addAll(fileMenu, runMenu);
        return menuBar;
    }

    private void toggleProjectSettingsSlideOut() {
        FXPGViewManager viewManager = FXPGFactory.viewManager();

        Pane projSetingsView = viewManager.getProjectSettingsView();
        if (((BooleanProperty) projSetingsView.getUserData()).get()) {
            // already open, so close it
            TranslateTransition translateTransition = new TranslateTransition(Duration.millis(300), projSetingsView);
            translateTransition.setFromX(0);
            translateTransition.setToX(-viewManager.getProjectSettingsView().getWidth());
            translateTransition.setOnFinished(t -> {
                projSetingsView.setVisible(false);
                // toggle project settings window to closed 'false'
                ((BooleanProperty) projSetingsView.getUserData()).set(false);
            });
            translateTransition.play();
        } else {
            // it's closed, so open it
            projSetingsView.setVisible(true);
            TranslateTransition translateTransition = new TranslateTransition(Duration.millis(300), projSetingsView);
            translateTransition.setFromX(-projSetingsView.getWidth());
            translateTransition.setToX(0);
            translateTransition.setOnFinished( ae -> {
                ((BooleanProperty) projSetingsView.getUserData()).set(true);
            });
            translateTransition.play();
        }

    }
    private void newPlayground(Playground newPlayground) {
        // unwire old playground
        FXPGFactory.currentPlayground().reset();
        unWireupUI();
        FXPGFactory.setCurrentPlayground(newPlayground);
        wireupUI();
    }
    private void dispatchRunCode() {
        Playground playground = FXPGFactory.currentPlayground();

        Language currentLang = playground.getCodeLanguage();
        if (currentLang == null) {
            currentLang = DEFAULT_LANGUAGE;
            playground.setCodeLanguage(DEFAULT_LANGUAGE);
        }
        if ("webkit".equalsIgnoreCase(currentLang.getScriptEngineName()) && "html".equalsIgnoreCase(playground.getMarkupLanguage())) {
            runHtml5Code();
        } else {
            runCode();
        }
    }
    private void runHtml5Code() {
        FXPGViewManager viewManager = FXPGFactory.viewManager();

        // take code and execute it.
        String cssContents = viewManager.getCssEditorText();
        String htmlContents = viewManager.getDomEditorText();
        String codeContents = viewManager.getLanguageEditorText();
        savePlayground();
        // clear subscene
        viewManager.getSplitViews().unwireOutput();
        BorderPane borderPane = new BorderPane();
        WebView webView = new WebView();
        webView.getEngine().setJavaScriptEnabled(true);

        webView.getEngine()
                .getLoadWorker()
                .stateProperty()
                .addListener( (obs, oldValue, newValue) -> {
                    if (newValue == Worker.State.SUCCEEDED) {
                        //JSObject jsobj = (JSObject) webView.getEngine().executeScript("window");
                        //jsobj.setMember("$MAIN", this);
                        webView.getEngine().executeScript(codeContents);

                    }
                });
        borderPane.setCenter(webView);
        Playground playground = FXPGFactory.currentPlayground();
        File playgroundDir = getPlaygroundDir(playground);
        File htmlFile = new File(playgroundDir.getAbsoluteFile() + File.separator + "markup.html");
        try {
            webView.getEngine().load(htmlFile.toURI().toURL().toString());
            //webView.getEngine().loadContent(htmlContents);
        } catch (MalformedURLException e) {
            logger.log(Level.SEVERE, "Unable to load URL: " + htmlFile, e);
        }
        viewManager.getSplitViews().wireOutput(borderPane);

    }
    private void runCode() {
        FXPGViewManager viewManager = FXPGFactory.viewManager();
        FXPGCodeManager codeManager = FXPGFactory.codeManager();
        // take code and execute it.
        String codeContents = viewManager.getLanguageEditorText();
        logger.log(Level.FINE, "Output = " + codeContents);

        Playground playground = FXPGFactory.currentPlayground();
        Language currentLang = playground.getCodeLanguage();
        logger.log(Level.FINE,"Run code's currentLang = " + currentLang);
        Map<String, Object> bindings = new HashMap<String, Object>();
        //bindings.put(DISPLAY_SCENE, viewManager.getOutputView());
        bindings.put(DISPLAY_SURFACE, viewManager.getOutputView().getRoot());
        ((Pane)viewManager.getOutputView().getRoot()).getChildren().clear();
        if (playground.getUseFXML()) {
            // load fxml in memory fxml file,
            InMemURLStreamHandler.registerURL(FX_PLAYGROUND_FXML, String.valueOf(viewManager.getDomEditorText()));
            try {
                Node fxmlNode = FXMLLoader.load(new URL(FX_PLAYGROUND_FXML));
                bindings.put(FXML_NODE, fxmlNode);
            } catch (IOException e) {
                logger.log(Level.SEVERE, "Unable to load fxml markup for " + playground.getName(), e);
                markupLogger.log(Level.SEVERE, "Unable to load fxml markup", e);
            }
        }
        // obtain all jars in project settings view
        viewManager.getProjectSettingsView();

        ClassLoader previousContextLoader = Thread.currentThread().getContextClassLoader();

        if (classLoaderCached == null) {
            try {
                URL[] urls = new URL[playground.getResources().size()];
                for (int i=0; i<urls.length; i++) {
                    URL url = null;
                    try {
                        urls[i]=new URL("jar:" + playground.getResources().get(i).getUrl() + "!/");
                    } catch (MalformedURLException e) {
                        logger.log(Level.SEVERE, "Unable to load jar resources for playground: " + playground.getName(), e);
                    }
                }

                classLoaderCached = URLClassLoader.newInstance(urls, previousContextLoader);
                for (int i=0; i< urls.length; i++) {
                    URL url = urls[i];
                    JarURLConnection uc = (JarURLConnection)url.openConnection();
                    Enumeration e = uc.getJarFile().entries();
                    while (e.hasMoreElements()) {
                        JarEntry je = (JarEntry) e.nextElement();
                        if (je.isDirectory() || !je.getName().endsWith(".class")) {
                            continue;
                        }
                        // chomp .class
                        String parseClassName = je.getName().substring(0, je.getName().length() - 6);
                        parseClassName = parseClassName.replace('/', '.');
                        logger.log(Level.FINE, parseClassName);
                        Class c = classLoaderCached.loadClass(parseClassName);
                    }
                }
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Unable to load jar resources for playground: " + playground.getName(), e);
            }
        }
        Platform.runLater(() -> {
            // add new class loader
            Thread.currentThread().setContextClassLoader(classLoaderCached);
            CompiledResult compiledResult = codeManager.compile(currentLang,
                    viewManager.getLanguageEditorText(),
                    bindings,
                    DISPLAY_SURFACE,
                    DISPLAY_SURFACE_NODE_ID,
                    classLoaderCached
            );
            // add previous class loader
            Thread.currentThread().setContextClassLoader(previousContextLoader);

            viewManager.clearErrorIndicator(LANGUAGE_EDITOR);
            if (compiledResult.getCompilerException() != null) {
                viewManager.addErrorIndicator(compiledResult.getCompilerException());
            }

            // update any output items
            Pane displaySurface = (Pane) compiledResult.getResult();
            if (displaySurface == null) {
                logger.log(Level.FINE, "user didn't create a Pane.");
                displaySurface  = new Pane();
            }
            logger.log(Level.FINE, "The user generated display surface " + displaySurface.hashCode());
            displaySurface.setId(DISPLAY_SURFACE_NODE_ID);
            viewManager.getSplitViews().unwireOutput();
            viewManager.getSplitViews().wireOutput(displaySurface);
            logger.log(Level.FINE, "SubScene's hash ===> " + viewManager.getOutputView().hashCode());
            //setting up the css
            viewManager.applyStylesheet(viewManager.getOutputView(),
                    FX_PLAYGROUND_CSS,
                    viewManager.getCssEditorText());
            // @TODO: THERE is confusion between subscene's root and display surface the user can create.
            //viewManager.getSplitViews().setOutputSubSceneRoot(displaySurface);
            //viewManager.getSplitViews().requestLayout();
            //ScenicViewBooter.show(displaySurface);
        });

    }
    private File getPlaygroundDir(Playground playground) {
        // detect user's home dir
        String userHome = System.getProperty("user.home");
        // mkdir a fxplaygrounds directory if one doesn't exist
        File fxplaygroundsDir = new File(userHome + File.separator + FX_PLAYGROUND_DIR);
        logger.log(Level.FINE, "Creating dirctory: " + fxplaygroundsDir);
        if (!fxplaygroundsDir.exists()) {
            boolean success = fxplaygroundsDir.mkdir();
            if (!success) {
                logger.log(Level.SEVERE, "Error trying create directory: " + fxplaygroundsDir.getAbsolutePath());
            }
        }
        // mkdir a playground subdirectory if one doesn't exist
        File playgroundDir = null;
        if (null == playground.getName() || playground.getName().trim().isEmpty()) {
            playgroundDir = new File(userHome + File.separator + FX_PLAYGROUND_DIR + File.separator + "current_playground");
        } else {
            playgroundDir = new File(userHome + File.separator + FX_PLAYGROUND_DIR + File.separator + playground.getName().replace(' ', '_'));
        }

        if (!playgroundDir.exists()) {
            boolean success = playgroundDir.mkdir();
            if (!success) {
                logger.log(Level.SEVERE, "Error trying create directory: " + playgroundDir.getAbsolutePath());
            }
        }
        return playgroundDir;
    }
    private void savePlayground() {
        Playground playground = FXPGFactory.currentPlayground();
        logger.log(Level.FINE, "playground = " + playground);
        primaryStage.setTitle("FX Playground" + " - " + playground.getName());
        File playgroundDir = getPlaygroundDir(playground);

        // store as a json object
        InputStream in = getClass().getResourceAsStream("/playground_template.tpl");
        String text = StreamUtil.streamToString(in, "UTF-8");

        // given a map of name value pair.
        Map<String, Object> bindings = new HashMap<>();
        bindings.put("name", playground.getName());
        bindings.put("description", TemplateUtil.escapeStringForJson(playground.getDescription()));
        bindings.put("useFXML", playground.getUseFXML());
        bindings.put("detachOutput", playground.getDetachOutput());
        StringBuilder resourceJsArrayBuf = new StringBuilder();
        playground.getResources().stream().map(fxpRes -> {
            StringBuilder sb = new StringBuilder();
            sb.append("{ name: '" + fxpRes.getName() + "', \n")
                    .append("  version: '" + fxpRes.getVersion() + "', \n")
                    .append("  url: '" + fxpRes.getUrl() + "' \n")
                    .append("}");
            return sb.toString();
        }).forEach(s -> {if (resourceJsArrayBuf.length() > 0) resourceJsArrayBuf.append(","); resourceJsArrayBuf.append(s);});

        bindings.put("resources", "[" +resourceJsArrayBuf.toString() + "]");
        if (playground.getCodeLanguage() == null) {
            playground.setCodeLanguage(FXPGFactory.codeManager().findByName("nashorn"));
        }
        bindings.put("codeLanguage", playground.getCodeLanguage().getScriptEngineName());


        if (playground.getMarkupLanguage() == null) {
            playground.setMarkupLanguage("FXML");
        }
        bindings.put("markupLanguage", playground.getMarkupLanguage());

        if ("HTML".equalsIgnoreCase(playground.getMarkupLanguage())) {
            bindings.put("markupTextFilename", "markup.html");
        } else {
            bindings.put("markupTextFilename", "markup.fxml");
        }

        bindings.put("codeTextFilename", "code." + playground.getCodeLanguage().getFileExtension());
        bindings.put("cssTextFilename", "style.css");

        String result = TemplateUtil.transform(text, bindings);
        logger.log(Level.FINE, result);
        try {
            FileWriter fs = new FileWriter(playgroundDir + File.separator + "playground.json");
            BufferedWriter out = new BufferedWriter(fs);
            out.write(result);
            out.flush();
            out.close();
        } catch (Exception e){
            throw new RuntimeException(e);
        }
        FXPGViewManager viewManager = FXPGFactory.viewManager();

        try {
            FileWriter fs = new FileWriter(playgroundDir + File.separator + "markup." + playground.getMarkupLanguage().toLowerCase());
            BufferedWriter out = new BufferedWriter(fs);
            out.write(viewManager.getDomEditorText());
            out.flush();
            out.close();
        } catch (Exception e){
            throw new RuntimeException(e);
        }
        try {
            FileWriter fs = new FileWriter(playgroundDir + File.separator + "code." + playground.getCodeLanguage().getFileExtension());
            BufferedWriter out = new BufferedWriter(fs);
            out.write(viewManager.getLanguageEditorText());
            out.flush();
            out.close();
        } catch (Exception e){
            throw new RuntimeException(e);
        }

        try {
            FileWriter fs = new FileWriter(playgroundDir + File.separator + "style.css");
            BufferedWriter out = new BufferedWriter(fs);
            out.write(viewManager.getCssEditorText());
            out.flush();
            out.close();
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    /**
     * This gets removed and added to the language editor pane.
     */
    private ChangeListener updateEditorListener = null;

    private void openPlayground() {
        logger.entering(this.getClass().getName(), "openPlayground");
        // load project
        String userHome = System.getProperty("user.home");
        File fxplaygroundsDir = new File(userHome + File.separator + FX_PLAYGROUND_DIR);
        logger.log(Level.FINE, "Creating.. " + fxplaygroundsDir);
        if (!fxplaygroundsDir.exists()) {
            boolean success = fxplaygroundsDir.mkdir();
            if (!success) {
                logger.log(Level.SEVERE, "Error trying create directory: " + fxplaygroundsDir.getAbsolutePath());
            }
        }

        DirectoryChooser directoryChooser = new DirectoryChooser();

        directoryChooser.setTitle("Open a Playground");

        if (fxplaygroundsDir != null) {
            directoryChooser.setInitialDirectory(fxplaygroundsDir);
        }

        File selectedDirectory = directoryChooser.showDialog(primaryStage);
        if (selectedDirectory != null) {
            String name = null;
            // validate and locate if a fxplayground.json file exists.
            File projectFile = new File(selectedDirectory.getAbsolutePath() + File.separator + "playground.json");
            // @TODO use dialogs to tell the user the selected directory is not a valid FX Playground.
            if (!projectFile.exists()) throw new RuntimeException("Not an FX Playground project directory");

            logger.log(Level.FINE, "Opening: " + projectFile.toString());

            // create a Playground instance
            // read json file into playground instance
            ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
            ScriptEngine nashorn = scriptEngineManager.getEngineByName("nashorn");
            //InputStream fis = null;
            Playground playground = new Playground();
            // unwire old UI
            // wire up new playground
            FXPGFactory.currentPlayground().reset();
            unWireupUI();
            FXPGFactory.setCurrentPlayground(playground);
            wireupUI();
            try (InputStream fis = new FileInputStream(projectFile) ){
                //fis = new FileInputStream(projectFile);
                String jsonText = StreamUtil.streamToString(fis, "utf-8");
                logger.log(Level.FINE, "fxproj: " + jsonText);

                nashorn.getBindings(ScriptContext.ENGINE_SCOPE).put("playground", playground);
                nashorn.getBindings(ScriptContext.ENGINE_SCOPE).put("FXPGCodeManager", FXPGFactory.codeManager());
                StringBuilder sb = new StringBuilder();
                sb.append("var p=" + jsonText + ";\n");
                sb.append("playground.setName(p.name);\n")
                        .append("playground.setDescription(p.description);\n")
                        .append("playground.setUseFXML(p.useFXML);\n")
                        .append("playground.setDetachOutput(p.detachOutput);\n")
                        .append("playground.setCodeLanguage(FXPGCodeManager.findByName(p.codeLanguage));\n")
                        .append("var FXPGResource = Java.type(\"org.fxplayground.beans.FXPGResource\");\n")
                        .append("p.resources.forEach( function(res) {\n" +
                                "   var fxpgRes = new FXPGResource(res.name, res.version, res.url);\n" +
                                "   playground.getResources().add(fxpgRes);\n" +
                                "});\n")
                        .append("playground.setMarkupLanguage(p.markupLanguage);\n");
                sb.append("playground.setMarkupTextFilename(p.markupTextFilename);\n")
                        .append("playground.setCodeTextFilename(p.codeTextFilename);\n")
                        .append("playground.setCssTextFilename(p.cssTextFilename);\n");

                nashorn.eval(sb.toString());
                FXPGViewManager viewManager = FXPGFactory.viewManager();
                logger.log(Level.FINE, "The loaded playground's name " + playground.getName());
                File markupTextFile = new File(selectedDirectory.getAbsolutePath() + File.separator + playground.getMarkupTextFilename());

                try (InputStream markupFileStream = markupTextFile.toURI().toURL().openStream()) {
                    String markupText = StreamUtil.streamToString(markupFileStream, "utf-8");
                    viewManager.setDomEditorText(markupText, playground.getMarkupLanguage());
                }
                File codeTextFile = new File(selectedDirectory.getAbsolutePath() + File.separator + playground.getCodeTextFilename());

                try (InputStream codeFileStream = codeTextFile.toURI().toURL().openStream()) {
                    String codeText = StreamUtil.streamToString(codeFileStream, "utf-8");

                    viewManager.setLanguageEditorText(codeText, playground.getCodeLanguage());

                }
                File cssTextFile = new File(selectedDirectory.getAbsolutePath() + File.separator + playground.getCssTextFilename());

                try (InputStream cssFileStream = cssTextFile.toURI().toURL().openStream()) {
                    String cssText = StreamUtil.streamToString(cssFileStream, "utf-8");
                    viewManager.setCssEditorText(cssText);
                }
                primaryStage.setTitle("FX Playground" + " - " + playground.getName());
            } catch (FileNotFoundException e) {
                logger.log(Level.SEVERE, "Unable to load playground: " + playground.getName(), e);
            } catch (ScriptException e) {
                logger.log(Level.SEVERE, "Unable to load playground: " + playground.getName(), e);
            } catch (IOException e) {
                logger.log(Level.SEVERE, "Unable to load playground: " + playground.getName(), e);
            } finally {

            }
        }
    }

}
