# README #

Welcome to FX Playground!
This project allows a JavaFX or Html5 developer to run code on the fly without needing to compile a Java project.

After cloning the project navigate to the subdirectory **fxplayground** to perform the gradle build. To build and run the project you should be in the same directory where the build.gradle file is located. 

### Prerequest (JDK8u20 or greater, Gradle 2.x or greater) ###

1. Install JDK 8
2. Set your JAVA_HOME
3. Install Gradle
4. Set your GRADLE_HOME environment variable
5. git clone git@bitbucket.org:cdea/fxplayground.git


You can build the project by calling 

```

gradle clean build
```

### Run the application ###

```

gradle run
```

### How to create a build to be distributed (for an end user)? ###
```

gradle distAll
```
You will get two builds for Windows and a cross platform version of FX Playground as a zip file.

```
drwxr-xr-x    1 cdea     Administ     4096 Oct 19 23:39 FXPlayground
-rw-r--r--    1 cdea     Administ 85784869 Oct 19 23:40 fxplayground-1.0-SNAPSHOT.zip

```

The **FXPlayground** directory consists of a Windows executable for users to double click to launch. Shown in the figure below is the structure underneath the **FXPlayground** directory.
![windows-app.png](https://bitbucket.org/repo/ARa88q/images/1596912235-windows-app.png)

As a native executable for the Windows platform the advantages is having the JRE bundled, therefore the end user does not need to download and install a version of Java.
 
To provide a cross platform version to Linux, Mac or Windows the **fxplayground-1.0-SNAPSHOT.zip** file version is what you will want to use. Just unzip the file and launch the shell or bat file in the '**bin**' directory. The down side of the cross platform version is that it assumes you have the correct JRE installed such as JRE 8u20.
Shown below is the zip file expanded:

![windows-app2.png](https://bitbucket.org/repo/ARa88q/images/4244800426-windows-app2.png)

The contents of the bin directory shows various script files (**fxplayground.bat** and **fxplayground**) to launch at the command line or double clicked on a windowing platform such as Ubuntu, CentOS, Mac, Windows 7.

![windows-app3.png](https://bitbucket.org/repo/ARa88q/images/992360000-windows-app3.png)
### What is this repository for? ###

An application developer wanting to rapidly develop and execute code without recompiling. Allows the developer to prototype HTML5, JavaFX, Ruby, Groovy, Python, Scala, Clojure, CoffeeScript and JavaScript.

In the near feature the plan is to push a version of both distributions to be stored on the BinTray.com.

# Examples #
When finished cloning the project you should see a subdirectory **examples**. To load any of the examples into the FX Playground application you will simply Open (Ctrl/Command + O) by navigating to the examples directory and selecting a folder. After, loading select the Run menu option or Ctrl/Command + R.
The examples directory will grow as more examples are being created.

# Slides at JavaOne #
Please visit https://oracleus.activeevents.com/2014/connect/sessionDetail.ww?SESSION_ID=2730 to download the slides. Also you can get the slides [https://oracleus.activeevents.com/2014/connect/fileDownload/session/2B9F3143C08BA95518DCFBF458DB9963/BOF2730_Dea-BoF-2730.pptx](https://oracleus.activeevents.com/2014/connect/fileDownload/session/2B9F3143C08BA95518DCFBF458DB9963/BOF2730_Dea-BoF-2730.pptx)

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact