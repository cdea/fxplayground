load("fx:base.js");
load("fx:graphics.js");

DISPLAY_SURFACE = new Pane();

var onePath = new Path();
onePath.setId('my-path');
var anchorPt;

function startPath(x, y) {
  onePath.elements.clear();
  onePath.styleClass.remove('invisible');
  onePath.styleClass.add('visible');
  anchorPt = new Point2D(x, y);      // start point in path
  onePath.elements.add(new MoveTo(anchorPt.x, anchorPt.y));
}

function drawPath(x, y) {
  onePath.elements.add(new LineTo(x, y));
}

function endPath(pathTransition) {
    onePath.styleClass.remove('visible');
    onePath.styleClass.add('invisible');
    
    if (onePath.elements.size() > 1) {
        pathTransition.stop();
        pathTransition.playFromStart();
    }
}

DISPLAY_SURFACE.children.add(onePath); // add the path to record events
//Nice colorized gradient for the circle
var gradient1 = new RadialGradient(0,
        .1,
        100,
        100,
        20,
        false,
        CycleMethod.NO_CYCLE,
        new Stop(0, Color.RED),
        new Stop(1, Color.BLACK));

// create a circle to be animated
var circle = new Circle(100, 100, 20, gradient1);
// add circle to the scene
DISPLAY_SURFACE.children.add(circle);

// animate circle by following the path.
var pathTransition = 
    new PathTransition(Duration.millis(4000), onePath, circle);
pathTransition.cycleCount = 1;
pathTransition.orientation = PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT;
    
// once finished clear path
pathTransition.onFinished = function(actionEvent) {
    onePath.elements.clear(); 
};

// starting initial path
DISPLAY_SURFACE.onMousePressed = function(mouseEvent) {   
   startPath(mouseEvent.x, mouseEvent.y);
};

// dragging creates lineTo objects added to the path
DISPLAY_SURFACE.onMouseDragged = function(mouseEvent) {
    drawPath(mouseEvent.x, mouseEvent.y)
};

// end the path when mouse released event
DISPLAY_SURFACE.onMouseReleased = function (mouseEvent) { 
    endPath(pathTransition);
};
