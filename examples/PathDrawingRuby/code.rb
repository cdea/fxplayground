require 'java'

Color = javafx.scene.paint.Color
CycleMethod = javafx.scene.paint.CycleMethod
Duration = javafx.util.Duration
MoveTo = javafx.scene.shape.MoveTo
LineTo = javafx.scene.shape.LineTo
PathTransition = javafx.animation.PathTransition
RadialGradient = javafx.scene.paint.RadialGradient
Stop = javafx.scene.paint.Stop
OrientationType = javafx.animation.PathTransition::OrientationType

$DISPLAY_SURFACE = javafx.scene.layout.Pane.new
onePath = javafx.scene.shape.Path.new
onePath.id = 'my-path'
anchorPt = nil

startPath = lambda { |x, y| 
  onePath.elements.clear()
  onePath.styleClass.remove 'invisible'
  onePath.styleClass.add 'visible'
  # start point in path
  anchorPt = javafx.geometry.Point2D.new x, y 
  onePath.elements.add MoveTo.new(anchorPt.x, anchorPt.y)
}

drawPath = lambda { |x, y|
  onePath.elements.add LineTo.new(x, y)
}

endPath = lambda { |pathTransition|
    onePath.styleClass.remove 'visible'
    onePath.styleClass.add 'invisible'
    
    if onePath.elements.size() > 1 
        pathTransition.stop()
        pathTransition.playFromStart()
    end
}

# add the path to record events
$DISPLAY_SURFACE.children.add onePath
# Nice colorized gradient for the circle
gradient1 = RadialGradient.new(0, 0.1, 100, 100, 20, false,
        CycleMethod::NO_CYCLE,
        Stop.new(0, Color::RED),
        Stop.new(1, Color::BLACK))

# create a circle to be animated
circle = javafx.scene.shape.Circle.new(100.0, 100.0, 20.0, gradient1)

# add circle to the scene
$DISPLAY_SURFACE.children.add circle

# animate circle by following the path.
pathTransition = PathTransition.new(Duration.millis(4000), onePath, circle)
pathTransition.cycleCount = 1
pathTransition.orientation = OrientationType::ORTHOGONAL_TO_TANGENT
    
# once finished clear path
pathTransition.onFinished = lambda { |actionEvent| 
    onePath.elements.clear() 
}

# starting initial path
$DISPLAY_SURFACE.onMousePressed = lambda { |mouseEvent| 
   startPath.call(mouseEvent.x, mouseEvent.y)
}

# dragging creates lineTo objects added to the path
$DISPLAY_SURFACE.onMouseDragged = lambda { |mouseEvent|
    drawPath.call(mouseEvent.x, mouseEvent.y)
}

# end the path when mouse released event
$DISPLAY_SURFACE.onMouseReleased = lambda { |mouseEvent|
    endPath.call(pathTransition)
}